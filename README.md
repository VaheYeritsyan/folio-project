---

# Folio Project - Spring Boot Application

This repository contains the backend implementation for the Folio task. 

## Getting Started

### Prerequisites

- Java 17
- Maven

### Running the Application

To run the Spring Boot application, navigate to the root directory of the project and execute the following command:

```bash
mvn spring-boot:run
```

The application will start and by default will be accessible at `http://localhost:8080`.

## Documentation

### Swagger UI

you can access the Swagger UI for API documentation and testing:

[Swagger UI](http://localhost:8080/swagger-ui/index.html)

### Postman Collection

you can use the shared Postman collection to quickly get started with the available APIs:

[Postman Collection](https://api.postman.com/collections/10861627-f27819ae-8499-4e3f-8255-18329afd8cd5?access_key=PMAT-01H7ZE42STNBJQ8XK0EN5AA7VA)

