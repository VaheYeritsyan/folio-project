package com.vema.fds.controller;

import com.vema.fds.dto.BlogDto;
import com.vema.fds.repository.page.PageResponse;
import com.vema.fds.service.BlogService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/blogs")
@AllArgsConstructor
@Slf4j
public class BlogController {
    private final BlogService blogService;

    @GetMapping
    public ResponseEntity<PageResponse> getBlogs(@RequestParam(defaultValue = "0") Integer page,
                                                 @RequestParam(defaultValue = "10") Integer size,
                                                 @RequestParam(required = false) String tags) {
        log.info("Getting Blogs: page={}, size={}, tags={}", page, size, tags);
        return ResponseEntity.ok(blogService.getBlogs(page, size, tags));
    }

    @PostMapping
    public ResponseEntity<BlogDto> create(@Valid @RequestBody BlogDto blogDto) {
        log.info("Creating Blog: {}", blogDto);
        return new ResponseEntity<>(blogService.createBlog(blogDto), HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BlogDto> updateBlogTags(@RequestBody List<String> tags, @PathVariable("id") UUID id) {
        log.info("Updating Blog tags for: {}", id);
        return ResponseEntity.ok(blogService.updateBlogTags(tags, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BlogDto> delete(@PathVariable("id") UUID id) {
        log.info("Deleting Blog: {}", id);
        blogService.delete(id);
        return ResponseEntity.ok().build();
    }

}
