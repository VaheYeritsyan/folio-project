package com.vema.fds.dto;

import jakarta.validation.constraints.NotEmpty;

import java.util.List;
import java.util.UUID;

public record BlogDto(UUID id, @NotEmpty String title, @NotEmpty String content, List<String> tags) {

}
