package com.vema.fds.exception;

import jakarta.persistence.EntityNotFoundException;
import lombok.NoArgsConstructor;

import java.util.UUID;
import java.util.function.Supplier;

@NoArgsConstructor(access = lombok.AccessLevel.PRIVATE)
public class ExceptionHelper {

    public static Supplier<EntityNotFoundException> getEntityNotFoundException(UUID uuid) {
        return () -> new EntityNotFoundException(String.format("Entity with id %s not found", uuid));
    }

    public static Supplier<EntityNotFoundException> getEntityNotFoundException(String fieldName, String value) {
        return () -> new EntityNotFoundException(String.format("Entity with %s %s not found", fieldName, value));
    }

    public static Supplier<EntityNotFoundException> getEntityNotFoundException() {
        return () -> new EntityNotFoundException("No entities match your request");
    }
}
