package com.vema.fds.exception;

import com.vema.fds.exception.error.ApiError;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import static com.vema.fds.exception.ResponseErrorsUtil.ERROR_MESSAGES;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        ApiError apiError = buildApiError(BAD_REQUEST, "Validation Error", ex);
        apiError.addFieldErrors(ex.getBindingResult().getFieldErrors());
        apiError.addGlobalErrors(ex.getBindingResult().getGlobalErrors());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler({
            RuntimeException.class
    })
    public ResponseEntity<Object> handleIllegalArgument(RuntimeException ex) {
        return buildResponseEntity(buildApiError(BAD_REQUEST, ex));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handleEntityNotFound(EntityNotFoundException ex) {
        return buildResponseEntity(buildApiError(NOT_FOUND, ex));
    }

    @ExceptionHandler({
            MethodArgumentTypeMismatchException.class
    })
    public ResponseEntity<Object> handleArgumentTypeMismatch(RuntimeException ex) {
        String name = ((MethodArgumentTypeMismatchException) ex).getName();
        ApiError apiError = buildApiError(BAD_REQUEST, "Invalid value for query parameter: " + name, ex);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(IllegalStateException.class)
    public ResponseEntity<Object> handleIllegalState(IllegalStateException ex) {
        return buildResponseEntity(buildApiError(BAD_REQUEST, ex));
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleGenericException(Exception ex) {
        return buildResponseEntity(buildApiError(INTERNAL_SERVER_ERROR, ex.getMessage(), ex));
    }

    private ApiError buildApiError(@Nullable HttpStatus status, String message, Exception ex) {
        if (status == null || status == INTERNAL_SERVER_ERROR) {
            return ApiError.from(INTERNAL_SERVER_ERROR, ERROR_MESSAGES.get(INTERNAL_SERVER_ERROR).getMessage());
        }
        return ApiError.from(status, message);
    }

    private ApiError buildApiError(HttpStatus status, Exception ex) {
        return ApiError.from(status, ex.getMessage());
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        log.error("Api error of type {} occurred, Id: {}, Message : {}", apiError.getType(), apiError.getId(), apiError.getMessage());
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }
}
