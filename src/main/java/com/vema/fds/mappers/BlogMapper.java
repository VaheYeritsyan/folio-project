package com.vema.fds.mappers;

import com.vema.fds.dto.BlogDto;
import com.vema.fds.entity.Blog;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BlogMapper {
    @Mapping(target = "tags", source = "tags", qualifiedByName = "listToString")
    Blog toEntity(BlogDto blogDto);

    @Mapping(target = "tags", source = "tags", qualifiedByName = "stringToList")
    BlogDto toDto(Blog blog);

    @Named("listToString")
    default String joinListWithDelimiter(List<String> list) {
        if (list == null) {
            return null;
        }
        return String.join(",", list);
    }

    @Named("stringToList")
    default List<String> splitStringToList(String str) {
        if (str == null) {
            return null;
        }
        return List.of(str.split(","));
    }
}

