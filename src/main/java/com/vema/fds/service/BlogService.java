package com.vema.fds.service;

import com.vema.fds.dto.BlogDto;
import com.vema.fds.entity.Blog;
import com.vema.fds.mappers.BlogMapper;
import com.vema.fds.repository.BlogRepository;
import com.vema.fds.repository.page.PageDetails;
import com.vema.fds.repository.page.PageResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class BlogService {
    private final BlogRepository blogRepository;
    private final BlogMapper blogMapper;

    public PageResponse getBlogs(Integer page, Integer size, String tags) {
        log.info("getBlogs: page={}, size={}", page, size);
        Page<Blog> blogs;
        if (tags == null || tags.isBlank()) {
            blogs = blogRepository.findAll(PageRequest.of(page, size));
        } else {
            blogs = blogRepository.findByTagsContaining(PageRequest.of(page, size), tags);
        }
        List<BlogDto> dtoList = blogs.getContent()
                .stream()
                .map(blogMapper::toDto)
                .toList();
        return PageResponse.builder()
                .page(PageDetails.builder()
                        .size(blogs.getSize())
                        .totalElements(blogs.getTotalElements())
                        .totalPages(blogs.getTotalPages())
                        .number(blogs.getNumber())
                        .build())
                .data(dtoList)
                .build();
    }

    public BlogDto createBlog(BlogDto blogDto) {
        log.info("createBlog: {}", blogDto);
        Blog entity = blogMapper.toEntity(blogDto);
        return blogMapper.toDto(blogRepository.save(entity));
    }

    public BlogDto updateBlogTags(List<String> tags, UUID id) {
        log.info("updateBlogTags: {}", id);
        Blog blog = blogRepository.findById(id).orElseThrow(() -> new RuntimeException("Blog not found"));
        blog.setTags(joinTags(tags));
        return blogMapper.toDto(blogRepository.save(blog));
    }

    public void delete(UUID id) {
        log.info("delete blog: {}", id);
        blogRepository.deleteById(id);
    }

    private String joinTags(List<String> tags) {
        return String.join(",", tags);
    }
}
